﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asphalt;
using Eco.Core.Plugins.Interfaces;
using Eco.Gameplay.Economy;
using GlobalCurrency.injection;
using Harmony;

namespace GlobalCurrency
{
    [AsphaltPlugin]
    public class GlobalCurrency : IModKitPlugin
    {
        public static string prefix = "GlobalCurrency: ";

        public void OnPostEnable()
        {
            printMessage("Plugin started.");
            Asphalt.Api.Asphalt.Harmony.Patch(BankAccountInjection.TargetMethod(), new HarmonyMethod(typeof(BankAccountInjection), "Prefix"));
            Asphalt.Api.Asphalt.Harmony.Patch(GetPlayerCurrencyInjection.TargetMethod(), new HarmonyMethod(typeof(GetPlayerCurrencyInjection), "Prefix"));
            Asphalt.Api.Asphalt.Harmony.Patch(NewPlayerCurrency.TargetMethod(), new HarmonyMethod(typeof(NewPlayerCurrency), "Prefix"));

            printMessage("Plugin laoded.");
        }

        public string GetStatus()
        {
            return "GlobalCurrency started";
        }

        public static void printMessage(string message)
        {
            Console.WriteLine($"{prefix} {message}");
        }
    }
}
