﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Eco.Gameplay.Economy;
using Eco.Gameplay.Players;
using Eco.Shared.Items;
using Harmony;

namespace GlobalCurrency.injection
{
    public class BankAccountInjection
    {
        public static System.Reflection.MethodBase TargetMethod()
        {
            return typeof(BankAccountManager).GetMethod("TryCreateUserAccount", BindingFlags.Instance | BindingFlags.Public);
        }

        public static bool Prefix(BankAccountManager __instance, User user)
        {
            if (user == null) return false;

            var persoAccounts = __instance.GetType()
                .GetFields(BindingFlags.Instance | BindingFlags.NonPublic)
                .FirstOrDefault(f => f.Name.Contains("personalAccounts"))
                .GetValue(__instance) as Dictionary<string, BankAccount>;


            // Create a personal bank account for them with infinite amount of that currency.
            if (!persoAccounts.ContainsKey(user.Name) && __instance.BankAccounts.All(x => x.Value.PersonalAccountName != user.Name))
            {
                var account = __instance.BankAccounts.Add(null) as BankAccount;
                account.Name = __instance.PlayerAccountName(user.Name);
                account.PersonalAccountName = user.Name;
                account.SpecialAccount = SpecialAccountType.Personal;
                account.DualPermissions.Managers.Add(user);


                var cur = CurrencyManager.Obj.Currencies.FirstOrDefault(c => c.CurrencyName == "Eco");
                account.CurrencyHoldings.Add(cur.Id, new CurrencyHolding() { Currency = cur, Val = 500});
                persoAccounts[user.Name] = account;
            }

            return false;
        }
    }
}
