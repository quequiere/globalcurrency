﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Eco.Gameplay.Economy;
using Harmony;

namespace GlobalCurrency.injection
{
    public class GetPlayerCurrencyInjection
    {
        public static System.Reflection.MethodBase TargetMethod()
        {
            return typeof(CurrencyManager).GetMethod("GetPlayerCurrency", BindingFlags.Instance | BindingFlags.Public);
        }

        public static bool Prefix(CurrencyManager __instance, ref Currency __result)
        {
            __result = __instance.Currencies.FirstOrDefault(c => c.CurrencyName == "Eco");
            return false;
        }
    }
}
