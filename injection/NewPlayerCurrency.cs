﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Eco.Core.Serialization;
using Eco.Gameplay.Components;
using Eco.Gameplay.Economy;
using Eco.Gameplay.Items;
using Eco.Gameplay.Players;
using Eco.Mods.TechTree;
using Eco.Shared.Items;
using Harmony;

namespace GlobalCurrency
{
    public class NewPlayerCurrency
    {
        public static System.Reflection.MethodBase TargetMethod()
        {
            return typeof(CurrencyManager).GetMethod("TryCreateUserAccount", BindingFlags.Instance | BindingFlags.Public);
        }

        public static bool Prefix(CurrencyManager __instance, User user)
        {
            if (__instance.Currencies.Count <= 0)
            {
                var currency = __instance.AddCurrency(user.Name, "Eco", CurrencyType.Backed);
                currency.BackingItem = Item.Get<GoldIngotItem>();
                currency.CoinsPerItem = 1;
                EconomyManager.Obj.Save();
                GlobalCurrency.printMessage("Created a global currency.");
            }

  

            return false;
        }
    }
}
